// http://stackoverflow.com/questions/4723213/detect-http-or-https-then-force-https-in-javascript
if (document.location.hostname != "localhost" && window.location.protocol != "https:") {
  window.location.href = "https:" + window.location.href.substring(window.location.protocol.length)
}

navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia)
