/**
 * Cosine windows
 */
let Windows = (function() {
  function Window(name, a, length) {
    var fn = makeWindowFn(a);
    var w = new Float32Array(length);
    for (var n = 0; n < length; n++) {
      w[n] = fn(n, length);
    }
    this.name = name;
    this.fn = w;
  }

  Window.prototype.doWindow = function doWindow(output, input) {
    for (var n = 0; n < input.length; n++) {
      output[n] = input[n] * this.fn[n];
    }
  };

  function makeWindowFn(coeffs) {
    return function windowFn(n, N) {
      return coeffs.map(function(a, k) {
        return a * Math.cos(2 * Math.PI * k * n / (N - 1));
      }).reduce(function(x, y) {
        return x + y;
      });
    };
  }

  function makeWindow(name, a) {
    return Window.bind(null, name, a);
  }

  var Windows = {
    Rect: makeWindow('rect', [1]),
    Hamming: makeWindow('hamming', [25/46, -21/46]),
    Blackman: makeWindow('blackman', [7938/18608, -9240/18608, 1430/18608]),
    BlackmanHarris: makeWindow('blackmanharris', [0.35875, -0.48829, 0.14128, -0.01168])
  };
  return Windows;
})()

/**
 * Enumeration of channel numbers
 */
let quadChan = (function() {
  return {
    L: 0, // left
    R: 1, // right
    LS: 2, // left shifted
    RS: 3, // right shifted
    LR: 2, // number of channels in stereo
    QUAD: 4 // number of channels in quadrature
  };
})()


/**
 * Processes stereo signals and outputs 90˚phase shifted signal
 * and a properly delayed unphase-shifted signal
 */
let HilbertNode = (function(w, C) {

  function HilbertNode(audio) {
    this.createBuffer = function createBuffer(N) {
      return audio.createBuffer(C.LR, N, audio.sampleRate);
    };
    // delay node has additional smoothing processing so do this instead
    this.unphasedNode = audio.createConvolver();
    this.phasedNode = audio.createConvolver();
    this.unphasedNode.normalize = false;
    this.phasedNode.normalize = false;
  }
  /* don't call this too often, making buffers/arrays take a while */
  HilbertNode.prototype.setFilterLength = function(N) {
    if (N < 2) {
      this.setNoFilter();
      return;
    }
    // N should be odd length
    var M = Math.floor(N / 2);
    N = M * 2 + 1;

    // delay unphased
    var delayIR = this.createBuffer(M);
    delayIR.getChannelData(C.L)[M-1] = 1;
    delayIR.getChannelData(C.R)[M-1] = 1;
    this.unphasedNode.buffer = delayIR;

    // generate phased IR
    var hilbertIR = this.createBuffer(N);
    var left = hilbertIR.getChannelData(C.L),
      right = hilbertIR.getChannelData(C.R);
    for (var i = 0; i < N; i++) {
      var n = i - M;
      var f_n = 2 / (Math.PI * n) * Math.abs(n % 2);
      left[i] = f_n;
      right[i] = f_n;
    }
    left[M] = 0;
    right[M] = 0;
    var hamming = new w.Hamming(N);
    hamming.doWindow(left, left);
    hamming.doWindow(right, right);
    this.phasedNode.buffer = hilbertIR;
  };
  HilbertNode.prototype.setNoFilter = function() {
    var buf = this.createBuffer(1);
    buf.getChannelData(C.L)[0] = 1;
    buf.getChannelData(C.R)[0] = 1;
    this.unphasedNode.buffer = buf;
    this.phasedNode.buffer = buf;
  };
  return HilbertNode;
})(Windows, quadChan)

/**
 * Quadrature processor node: takes stereo in
 * and gives time domain out with 90˚ phase shift pairs
 * Due to a non-flat magnitude response, it is recommended to
 * use +-45˚ phase shift pairs using these quad signals
 */
window.QuadProcessor = (function(HilbertNode, C) {

  function QuadProcessor(audio, bufLength, onProcess) {
    this.hilbert = new HilbertNode(audio);
    // apparently it gets gc'd if not referenced...?
    this.processor = audio.createScriptProcessor(bufLength, C.QUAD);
    var merger = audio.createChannelMerger(C.QUAD);
    merger.connect(this.processor);

    var unphasedSplitter = audio.createChannelSplitter(C.LR);
    this.hilbert.unphasedNode.connect(unphasedSplitter);
    unphasedSplitter.connect(merger, C.L, C.L);
    unphasedSplitter.connect(merger, C.R, C.R);

    var phasedSplitter = audio.createChannelSplitter(C.LR);
    this.hilbert.phasedNode.connect(phasedSplitter);
    phasedSplitter.connect(merger, C.L, C.LS);
    phasedSplitter.connect(merger, C.R, C.RS);

    this.processor.onaudioprocess = onProcess;
    this.processor.connect(audio.destination); // to make it call onProcess
  }
  QuadProcessor.prototype.connectToSource = function(source) {
    source.connect(this.hilbert.unphasedNode);
    source.connect(this.hilbert.phasedNode);
  };
  /**
   * sets the overall delay (in samples)
   * more delay means flatter magnitude response for hilbert
   */
  QuadProcessor.prototype.setDelay = function(delay) {
    var N = delay * 2 + 1;
    this.hilbert.setFilterLength(N);
  };
  return QuadProcessor;
})(HilbertNode, quadChan)
